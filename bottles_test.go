package main

import "testing"

func TestBottles(t *testing.T) {

	t.Run("no bottles", func(t *testing.T) {
		got := Bottles(0)
		want := "No more bottles of beer on the wall, no more bottles of beer.\n Go to the store and buy some more, 99 bottles of beer on the wall."

		assertEquals(t, got, want)
	})

	t.Run("one bottle", func(t *testing.T) {
		got := Bottles(1)
		want := "1 bottle of beer on the wall, 1 bottle of beer.\n Take it down and pass it around, no more bottles of beer on the wall."

		assertEquals(t, got, want)
	})

	t.Run("runs out of bottles", func(t *testing.T) {
		bottles := XBottles{1} // XBottles.new(1)
		bottles.Do()

		got := bottles.Do()
		want := "No more bottles of beer on the wall, no more bottles of beer.\n Go to the store and buy some more, 99 bottles of beer on the wall."

		assertEquals(t, got, want)
	})
}

func assertEquals(t *testing.T, got, want string) {
	t.Helper()

	if got != want {
		t.Errorf("got '%s' want '%s'", got, want)
	}
}
