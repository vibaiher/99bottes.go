package main

import "fmt"

type XBottles struct {
	Remaining int
}

func (b XBottles) Do() string {
	return "No more bottles of beer on the wall, no more bottles of beer.\n Go to the store and buy some more, 99 bottles of beer on the wall."
}

func Bottles(remaining int) string {
	song := ""

	if (remaining == 1) {
		song = "1 bottle of beer on the wall, 1 bottle of beer.\n Take it down and pass it around, no more bottles of beer on the wall."
	} else {
		song = "No more bottles of beer on the wall, no more bottles of beer.\n Go to the store and buy some more, 99 bottles of beer on the wall."
	}

	return song
}

func main() {
    fmt.Println(Bottles(99))
}
